<?php
/*
Plugin Name: Simple Audio RSS Reader
Plugin URI: 
Description: A simple widget to display audio rss.
Version: 1.0.0
Author: Diaz, Aimar
Author URI: https://betavulgariscicla.com
License: GPL3
*/

require_once( ABSPATH . WPINC . '/feed.php' );


/**
 * SimpleAudioRssReader_Widget Class
 */
class SimpleAudioRssReader_Widget extends WP_Widget{
	/** Constructor */
	function SimpleAudioRssReader_Widget(){
		parent::WP_Widget(
			'simpleaudiorssreader',
			__( 'Simple Audio RSS Reader', 'simpleaudiorssreader' ),
			array(	'classname' => 'simpleaudiorssreader', 'description' => __( 'Display third party feeds', 'simpleaudiorssreader' ) )
		);
	}

	/** @see WP_Widget::form */
	function form( $instance ){

		$title = esc_attr( $instance[ 'title' ] );
		$rss = esc_attr( $instance[ 'rss' ] );
		$intro = esc_attr( $instance[ 'intro' ] );
		
		// Get parameters
		$maxDisplayedItemsPerSource = $instance[ 'maxDisplayedItemsPerSource' ];
		if( !isset( $maxDisplayedItemsPerSource ) || $maxDisplayedItemsPerSource <= 0 || $maxDisplayedItemsPerSource > 100 )
			$maxDisplayedItemsPerSource = 2;

		$maxDisplayedItemsInTotal = $instance[ 'maxDisplayedItemsInTotal' ];
		if( !isset( $maxDisplayedItemsInTotal ) || $maxDisplayedItemsInTotal <= 0 || $maxDisplayedItemsInTotal > 100 )
			$maxDisplayedItemsInTotal = 5;

		// Source
		$source = $instance[ 'source' ];
		if( $source != 'displayed' && $source != 'hidden' )
			$source = 'displayed';

		// Signature
		$signature = $instance[ 'signature' ];
		if( $signature != 'full' && $signature != 'short' && $signature != 'hidden' )
			$signature = 'hidden';

		?>
		<p>
			<label for="<?php echo( $this->get_field_id( 'title' ) ); ?>">
				<?php _e( 'Title:', 'simpleaudiorssreader' ); ?>
				<input class="widefat" id="<?php echo( $this->get_field_id( 'title' ) ); ?>" name="<?php echo( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo( $title ); ?>" />
			</label>
		</p>

		<p>
			<label for="<?php echo( $this->get_field_id( 'intro' ) ); ?>">
				<?php _e( 'Description:', 'simpleaudiorssreader' ); ?>
				<textarea cols="40" rows="5" class="widefat" id="<?php echo( $this->get_field_id( 'intro' ) ); ?>" name="<?php echo( $this->get_field_name( 'intro' ) ); ?>"><?php echo( $intro ); ?></textarea>
			</label>
		</p>

		<p>
			<label for="<?php echo( $this->get_field_id( 'rss' ) ); ?>">
				<?php _e( 'RSS feeds (one per line):', 'simpleaudiorssreader' ); ?>
				<textarea cols="40" rows="5" class="widefat" id="<?php echo( $this->get_field_id( 'rss' ) ); ?>" name="<?php echo( $this->get_field_name( 'rss' ) ); ?>"><?php echo( $rss ); ?></textarea>
			</label>
		</p>

		<p>
			<label for="<?php echo( $this->get_field_id( 'maxDisplayedItemsPerSource' ) ); ?>">
				<?php _e( 'Maximum displayed items from same source:', 'simpleaudiorssreader' ); ?>
				<input class="widefat" id="<?php echo( $this->get_field_id( 'maxDisplayedItemsPerSource' ) ); ?>" name="<?php echo( $this->get_field_name( 'maxDisplayedItemsPerSource' ) ); ?>" type="text" value="<?php echo( $maxDisplayedItemsPerSource ); ?>" />
			</label>
		</p>

		<p>
			<label for="<?php echo( $this->get_field_id( 'maxDisplayedItemsInTotal' ) ); ?>">
				<?php _e( 'Maximum displayed items in total:', 'simpleaudiorssreader' ); ?>
				<input class="widefat" id="<?php echo( $this->get_field_id( 'maxDisplayedItemsInTotal' ) ); ?>" name="<?php echo( $this->get_field_name( 'maxDisplayedItemsInTotal' ) ); ?>" type="text" value="<?php echo( $maxDisplayedItemsInTotal ); ?>" />
			</label>
		</p>

		<p>
			<label for="<?php echo( $this->get_field_id( 'source' ) ); ?>">
				<?php _e( 'RSS Feed Source:', 'simpleaudiorssreader' ); ?>
				<select name="<?php echo $this->get_field_name('source'); ?>" id="<?php echo $this->get_field_id('source'); ?>" class="widefat">
					<option value="displayed"<?php selected( $source, 'displayed' ); ?>><?php _e( 'Display source', 'simpleaudiorssreader' ); ?></option>
					<option value="hidden"<?php selected( $source, 'hidden' ); ?>><?php _e( 'Hide source', 'simpleaudiorssreader' ); ?></option>
				</select>
			</label>
		</p>

		<p>
			<label for="<?php echo( $this->get_field_id( 'signature' ) ); ?>">
				<?php _e( 'Widget Signature:', 'simpleaudiorssreader' ); ?>
				<select name="<?php echo $this->get_field_name('signature'); ?>" id="<?php echo $this->get_field_id('signature'); ?>" class="widefat">
					<option value="full"<?php selected( $signature, 'full' ); ?>><?php _e( 'Display full signature', 'simpleaudiorssreader' ); ?></option>
					<option value="short"<?php selected( $signature, 'short' ); ?>><?php _e( 'Display short signature', 'simpleaudiorssreader' ); ?></option>
					<option value="hidden"<?php selected( $signature, 'hidden' ); ?>><?php _e( 'Hide signature', 'simpleaudiorssreader' ); ?></option>
				</select>
			</label>
		</p>

		<?php
	}

	/** @see WP_Widget::update */
	function update( $new_instance, $old_instance )
	{
		// processes widget options to be saved
		$instance = $old_instance;

		$instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
		$instance[ 'intro' ] = strip_tags( $new_instance[ 'intro' ] );
		$instance[ 'rss' ] = strip_tags( $new_instance[ 'rss' ] );
		
		$instance[ 'maxDisplayedItemsPerSource' ] = $new_instance[ 'maxDisplayedItemsPerSource' ];
		if(    !is_numeric( $instance[ 'maxDisplayedItemsPerSource' ] )
			|| $instance[ 'maxDisplayedItemsPerSource' ] <= 0
			|| $instance[ 'maxDisplayedItemsPerSource' ] > 100 )
		{
			$instance[ 'maxDisplayedItemsPerSource' ] = 2;
		}
		
		$instance[ 'maxDisplayedItemsInTotal' ] = $new_instance[ 'maxDisplayedItemsInTotal' ];
		if(    !is_numeric( $instance[ 'maxDisplayedItemsInTotal' ] )
			|| $instance[ 'maxDisplayedItemsInTotal' ] <= 0
			|| $instance[ 'maxDisplayedItemsInTotal' ] > 100 )
		{
			$instance[ 'maxDisplayedItemsInTotal' ] = 5;
		}

		$instance[ 'signature' ] = $new_instance[ 'signature' ];
		if( $instance[ 'signature' ] != 'full' && $instance[ 'signature' ] != 'short' && $instance[ 'signature' ] != 'hidden' )
			$instance[ 'signature' ] = 'hidden';

		$instance[ 'source' ] = $new_instance[ 'source' ];
		if( $instance[ 'source' ] != 'displayed' && $instance[ 'source' ] != 'hidden' )
			$instance[ 'source' ] = 'displayed';

		return $instance;

	}

	static function sort( $a, $b )
	{
		if( $a->get_date( 'U' ) < $b->get_date( 'U' ) )
		{
			return 1;
		}

		if( $a->get_date( 'U' ) > $b->get_date( 'U' ) )
		{
			return -1;
		}
		
		return 0;
	}

	/** @see WP_Widget::widget */
	function widget( $args, $instance )
	{
		// outputs the content of the widget
		extract( $args );

		echo( $before_widget );

		// Get title
		$title = apply_filters( 'widget_title', $instance[ 'title' ] );
		echo( $before_title . $title . $after_title );

		// Get intro
		$title = apply_filters( 'widget_title', $instance[ 'intro' ] );
		echo( '<p>' . $title . '</p>' );

		// Get parameters
		$maxDisplayedItemsPerSource = $instance[ 'maxDisplayedItemsPerSource' ];
		if( !isset( $maxDisplayedItemsPerSource ) || $maxDisplayedItemsPerSource <= 0 || $maxDisplayedItemsPerSource > 100 )
			$maxDisplayedItemsPerSource = 2;

		$maxDisplayedItemsInTotal = $instance[ 'maxDisplayedItemsInTotal' ];
		if( !isset( $maxDisplayedItemsInTotal ) || $maxDisplayedItemsInTotal <= 0 || $maxDisplayedItemsInTotal > 100 )
			$maxDisplayedItemsInTotal = 5;

		// Get source
		$source = $instance[ 'source' ];
		if( $source != 'displayed' &&  $source != 'hidden' )
			$source = 'displayed';

		// Get signature
		$signature = $instance[ 'signature' ];
		if( $signature != 'full' && $signature != 'short' && $signature != 'hidden' )
			$instance[ 'signature' ] = 'hidden';

		// Get and display RSS feeds
		$rss = $instance[ 'rss' ];
		$rssUris  = explode( "\n", $rss );
		// parse feeds URIs
		$displayedItems = array();
		foreach( $rssUris as $rssUri )
		{
			// Is the URI fine?
			if( !empty( $rssUri ) )
			{
				// OK, fetch feed
				$feed = fetch_feed( $rssUri );
				if( !is_wp_error( $feed ) )
				{
					// OK
					$maxItems = $feed->get_item_quantity( $maxDisplayedItemsPerSource );
					if( $maxItems > 0 )
					{
						// OK, found items in the fetched feed
						$feedItems = $feed->get_items( 0, $maxItems );
						foreach( $feedItems as $item )
						{
							$displayedItems[] = $item;
						}
					}
				}
			}
		}

		// Do we have at least 1 item to display?
		if( !empty( $displayedItems ) )
		{
			// Yes, we do
			usort( $displayedItems, 'SimpleAudioRssReader_Widget::sort' );

			echo( '<ul>' );
			$displayedItemsCount = 0;
			foreach( $displayedItems as $item )
			{
				echo( '<li><a title="'.date_i18n( get_option( 'date_format' ), $item->get_date( 'U' ) ).'" href="'.$item->get_permalink().'">'.$item->get_title().'</a>' );
				if( $source != 'hidden' )
					/*echo( ' <small>(<cite>'.$item->get_feed()->get_title().'</cite>)</small>' );*/

					$allEnclosures = $item->get_enclosures();

					foreach( $allEnclosures as $enclosure ){

						$lastDotPosition = strrpos($enclosure->get_link(), ".");
						$fileType = substr($enclosure->get_link(), $lastDotPosition);	 
						
						if($fileType == ".mp3" || $fileType == ".ogg"){
							$audioLink = $enclosure->get_link();
						}
					}
			
					echo ("<audio controls>");
					echo	("<source src=".$audioLink." type='audio/ogg'>");
  					echo	("<source src=".$audioLink." type='audio/mpeg'>");
					echo	("Your browser does not support the audio element.");
					echo ("</audio>");
				echo( '</li>');
				
				$displayedItemsCount++;
				if( $displayedItemsCount > $maxDisplayedItemsInTotal )
				{
					break;
				}
			}
			echo( '</ul>' );
		}

		// Display signature
		if( $signature != 'hidden' )
		{
			echo( '<p><small>' );
			if( $signature == 'full' )
			{
				printf(
					__( 'Powered by %s supplied by %s', 'simpleaudiorssreader' ),
					'<a title="'.__( 'Simple Audio RSS Reader', 'simpleaudiorssreader' ).'" href="#">'.__( 'Simple Audio RSS Reader', 'simpleaudiorssreader' ).'</a>',
					'<a title="bourse" href="#">Ou Yeah</a>' );
			}
			else
			{
				printf(
					__( 'Powered by %s', 'simpleaudiorssreader' ),
					'<a title="'.__( 'Simple Audio RSS Reader', 'simpleaudiorssreader' ).'" href="#">'.__( 'Simple Audio RSS Reader', 'simpleaudiorssreader' ).'</a>' );
			}
			echo( '</small></p>' );
		}

		echo( $after_widget );
	}

} // SimpleAudioRssReader_Widget

function SimpleAudioRssReader_Widget_Register()
{
	load_plugin_textdomain( 'simpleaudiorssreader', false, dirname( plugin_basename( __FILE__ ) ) );
	return register_widget( "SimpleAudioRssReader_Widget" );
}

// register widget
add_action( 'widgets_init', 'SimpleAudioRssReader_Widget_Register' );

